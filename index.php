<?php
    session_start();
    require_once "inc/functions.php";
    $info="";
    $task=$_GET['task']??"report";
    $error=$_GET['error']??0;
    if("edit"==$task){
        if(!isAdmin() && !isEditor()){
            header('location: /learnPHP/CRUD/index.php?task=report');
        }
    }
    if("delete"==$task){
        if(!isAdmin()){
            header('location: /learnPHP/CRUD/index.php?task=report');
            die();
        }
        $id=filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
        deleteStudent($id);
        header('location: /learnPHP/CRUD/index.php?task=report');
    }
    if("seed"==$task){
        if(!isAdmin()){
            header('location: /learnPHP/CRUD/index.php?task=report');
            die();
        }
        seed();
        $info="Seeding is complete";
    }
    if(isset($_POST['submit'])){
        $fname=filter_input(INPUT_POST,'fname',FILTER_SANITIZE_STRING);
        $lname=filter_input(INPUT_POST,'lname',FILTER_SANITIZE_STRING);
        $roll=filter_input(INPUT_POST,'roll',FILTER_SANITIZE_STRING);
        $id=filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING);
        if($id){
            if($fname!="" && $lname!="" && $roll!=""){
                $result=updateStudent($id,$fname,$lname,$roll);
                if($result){
                    header('location: /learnPHP/CRUD/index.php?task=report');
                }else{
                    $error=1;
                }
            }
        }else{
            if($fname!="" && $lname!="" && $roll!=""){
                $result=addStudent($fname,$lname,$roll);
                if($result){
                    header('location: /learnPHP/CRUD/index.php?task=report');
                }elseif($roll<=0){
                    $error=2;
                }else{
                    $error=1;
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Form Example</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.css">
        <style>
            body {
                margin-top: 30px;
            }
        </style>
        <script src=assests/jquery.min.js id=jquery-core-js></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                    <h1>Project-2:CRUD</h1>
                    <p>A simple project to perform CRUD operations using plain files and PHP</p>
                    <?php include_once "inc/templates/nav.php"; ?>
                    <hr>
                    <?php
                    if($info!=""){
                        echo "<p>$info<p>";
                    }
                    ?>
                </div>
            </div>
            <?php if('1'==$error){?>
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                <blockquote>
                Duplicate Roll Number
                </blockquote>
                </div>
            </div>
            <?php } ?>
            <?php if('2'==$error){?>
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                <blockquote>
                Roll must be a positive number
                </blockquote>
                </div>
            </div>
            <?php } ?>
            <?php if('report'==$task){?>
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                <?php generateReport();?>
                </div>
            </div>
            <?php } ?>
            <?php if('add'==$task){?>
            <div class="row">
                <div class="column column-60 column-offset-20" >
                <form action="/learnPHP/CRUD/index.php?task=add" method="POST">
                <label for="fname"><em>First Name</em></label>
                <input type="text" name="fname" id="fname" value="<?php echo $fname;?>">
                <label for="lname"><em>Last Name</em></label>
                <input type="text" name="lname" id="lname" value="<?php echo $lname;?>">
                <label for="roll"><em>Roll</em></label>
                <input type="number" name="roll" id="roll" value="<?php echo $roll;?>">
                <button type="submit" class="button-primary" name="submit">Save</button>
                </form>
                </div>
            </div>
            <?php } ?>
            <?php if('edit'==$task){
                $id=filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
                $student=getStudent($id);
                if($student){?>
            <div class="row">
                <div class="column column-60 column-offset-20" >
                <form method="POST">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <label for="fname"><em>First Name</em></label>
                <input type="text" name="fname" id="fname" value="<?php echo $student['fname'];?>">
                <label for="lname"><em>Last Name</em></label>
                <input type="text" name="lname" id="lname" value="<?php echo $student['lname'];?>">
                <label for="roll"><em>Roll</em></label>
                <input type="number" name="roll" id="roll" value="<?php echo $student['roll'];?>">
                <button type="submit" class="button-primary" name="submit"><em>Update</em></button>
                </form>
                </div>
            </div>
            <?php } 
            }?>
        </div>
        <script type="text/javascript" src="assets/js/script.js"></script>
    </body>
</html>