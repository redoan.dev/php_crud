<?php
    session_start(
        [
        'cookie_lifetime'=>30,
    ]
);
    //session_destroy();
    $username=filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
    $password=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
    $error=false;
    $fp=fopen("C:\\xampp\\htdocs\\learnPHP\\CRUD\\data\\users.txt","r");
    if($username && $password){
        $_SESSION['loggedin'] = false;
        $_SESSION['user'] = false;
        $_SESSION['role'] = false;
        while($data=fgetcsv($fp)){
            if( $data[0] == $username && $data[1] == sha1($password)){
                $_SESSION['loggedin'] = true;
                $_SESSION['user'] = $username;
                $_SESSION['role'] = $data[2];
                header('location:index.php');
            }
        }
        if(!$_SESSION['loggedin']){
            $error=true;
        }
    }
    if(isset($_GET['logout'])){
        $_SESSION["loggedin"] = false;
        $_SESSION['user'] = false;
        $_SESSION['role'] = false;
        session_destroy();
        header('location:index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Authentication</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.css">
        <style>
            body{
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                    <h1>Simple auth example</h1>
                </div>
            </div>
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                    <?php
                        echo "Hello stranger! login below";
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <?php
                        if($error){
                            echo "<blockquote>Username and Password didn't match</blockquote>";
                        }
                        if( false== $_SESSION['loggedin'] ){
                    ?>
                            <form method="POST">
                                <label for="username"><em>Username</em></label>
                                <input type="text" name="username" id="username">
                                <label for="password"><em>Password</em></label>
                                <input type="password" name="password" id="password">
                                <button type="submit" name="submit" class="button-primary">Log In</button>
                            </form>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>