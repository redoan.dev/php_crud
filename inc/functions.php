<?php
define('DB_NAME','C:\xampp\htdocs\learnPHP\CRUD\data\db.txt');
function seed(){
    $data=array(
        array(
            'id'=>1,
            'fname'=>"hasin",
            'lname'=>"hyder",
            'roll'=>20,
        ),
        array(
            'id'=>2,
            'fname'=>"rakesh",
            'lname'=>"datta",
            'roll'=>17,
        ),
        array(
            'id'=>3,
            'fname'=>"jibon",
            'lname'=>"sarkar",
            'roll'=>13,
        ),
        array(
            'id'=>4,
            'fname'=>"kamran",
            'lname'=>"ahmed",
            'roll'=>9,
        ),
        array(
            'id'=>5,
            'fname'=>"shakib",
            'lname'=>"hasan",
            'roll'=>1,
        )
    );
    $serializedData=serialize($data);
    file_put_contents(DB_NAME,$serializedData,LOCK_EX);
}
function generateReport(){
    $serializedData=file_get_contents(DB_NAME);
    $students=unserialize($serializedData);
?>
<table>
    <tr>
        <th>Name</th>
        <th>Roll</th>
        <?php 
            if(isAdmin() || isEditor()):
        ?>
        <th width="25%">Action</th>
        <?php
            endif;
        ?>
    </tr>
    <?php
        $nStudent=[];
        $len=count($students);
        for($i=0;$i<$len;$i++){
            foreach($students as $offset=>$student){
                $minRoll=min(array_column($students,'roll'));
                if ($student['roll']==$minRoll){
                    array_push($nStudent,$student);
                    unset($students[$offset]);
                }
            }
        }
        foreach($nStudent as $student){
            ?>
    <tr>
        <td><?php printf("%s %s",$student['fname'],$student['lname']) ?></td>
        <td><?php printf("%s",$student['roll']) ?></td>
        <?php 
            if(isAdmin()):
        ?>
        <td><?php printf('<a href="/learnPHP/CRUD/index.php?task=edit&id=%s">Edit<a> | <a class="delete" href="/learnPHP/CRUD/index.php?task=delete&id=%s">Delete<a>',$student['id'],$student['id']) ?></td>
        <?php
            endif;
        ?>
        <?php 
            if(isEditor()):
        ?>
        <td><?php printf('<a href="/learnPHP/CRUD/index.php?task=edit&id=%s">Edit<a>',$student['id']) ?></td>
        <?php
            endif;
        ?>
    </tr>        
    <?php        
        }
    ?>
</table>
<?php
}
?>
<?php
function addStudent($fname,$lname,$roll){
    $found=false;
    $serializedData=file_get_contents(DB_NAME);
    $students=unserialize($serializedData);
    foreach($students as $_student){
        if($_student['roll']==$roll){
            $found=true;
            break;
        }
    }
    if(!$found){
        if($roll>0){
            $newId=getNewId($students);
            $student=array(
                'id'=>$newId,
                'fname'=>$fname,
                'lname'=>$lname,
                'roll'=>$roll
            );
            array_push($students,$student);
            $serializedData=serialize($students);
            file_put_contents(DB_NAME,$serializedData,LOCK_EX);
            return true;
        }
        return false;
    }
    return false;
}
function getStudent($id){
    $serializedData=file_get_contents(DB_NAME);
    $students=unserialize($serializedData);
    foreach($students as $student){
        if($student['id']==$id){
            return $student;
        }
    }
    return false;
}
function updateStudent($id,$fname,$lname,$roll){
    $found=false;
    $serializedData=file_get_contents(DB_NAME);
    $students=unserialize($serializedData);
    foreach($students as $_student){
        if($_student['roll']==$roll && $_student['id']!=$id){
            $found=true;
            break;
        }
    }
    if(!$found){
    $students[$id-1]['fname']=$fname;
    $students[$id-1]['lname']=$lname;
    $students[$id-1]['roll']=$roll;
    $serializedData=serialize($students);
    file_put_contents(DB_NAME,$serializedData,LOCK_EX);
    return true;
    }
    return false;
}
function deleteStudent($id){
    $serializedData=file_get_contents(DB_NAME);
    $students=unserialize($serializedData);
    foreach($students as $offset=>$student){
        if($student['id']==$id){
            unset($students[$offset]);
        }
    }
    $serializedData=serialize($students);
    file_put_contents(DB_NAME,$serializedData,LOCK_EX);
}
function getNewId($student){
    $maxId=max(array_column($student,'id'));
    return $maxId+1;
}
function isAdmin(){
    return ('admin'==$_SESSION['role']);
}
function isEditor(){
    return ('editor'==$_SESSION['role']);
}
?>