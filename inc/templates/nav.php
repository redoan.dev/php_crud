<div>
    <div class="float-left">
        <p>
        <?php 
            if(isAdmin() || isEditor()):
        ?>
        <a href="/learnPHP/CRUD/index.php?task=report">All Students</a> |
        <?php
            endif;
        ?>
        <a href="/learnPHP/CRUD/index.php?task=add">Add New Student</a> 
        <?php 
            if(isAdmin()):
        ?>
        |
        <a href="/learnPHP/CRUD/index.php?task=seed">Seed</a>
        <?php
            endif;
        ?>
        </p>
    </div>
    <div class="float-right">
        <p>
        <?php
        if(!$_SESSION["loggedin"]):
        ?>
            <a href="/learnPHP/CRUD/auth.php">Log In</a>
        <?php
        else:
        ?>
            <a href="/learnPHP/CRUD/auth.php?logout=true">Log Out(<?= $_SESSION['role'];?>)</a>
        <?php
        endif;
        ?>
        </p>
    </div>
</div>